# translation of plasma_applet_org.kde.plasma.clipboard.pot to Esperanto
# Copyright (C) 2014 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-workspace package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-05-26 19:45+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/ui/BarcodePage.qml:23
#, kde-format
msgid "QR Code"
msgstr "QR-kodo"

#: contents/ui/BarcodePage.qml:24
#, kde-format
msgid "Data Matrix"
msgstr "Data Matrix"

#: contents/ui/BarcodePage.qml:25
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: contents/ui/BarcodePage.qml:26
#, kde-format
msgid "Code 39"
msgstr "Kodo 39"

#: contents/ui/BarcodePage.qml:27
#, kde-format
msgid "Code 93"
msgstr "Kodo 93"

#: contents/ui/BarcodePage.qml:28
#, kde-format
msgid "Code 128"
msgstr "Kodo 128"

#: contents/ui/BarcodePage.qml:44
#, kde-format
msgid "Return to Clipboard"
msgstr "Reiri al Tondujo"

#: contents/ui/BarcodePage.qml:79
#, kde-format
msgid "Change the QR code type"
msgstr "Ŝanĝi la tipon de QR-kodo"

#: contents/ui/BarcodePage.qml:136
#, kde-format
msgid "Creating QR code failed"
msgstr "Kreado de QR-kodo malsukcesis"

#: contents/ui/BarcodePage.qml:148
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr ""
"Ne estas sufiĉa spaco por montri la QR-kodon. Provu aligrandigi ĉi apleton."

#: contents/ui/DelegateToolButtons.qml:26
#, kde-format
msgid "Invoke action"
msgstr "Alvoki agon"

#: contents/ui/DelegateToolButtons.qml:41
#, kde-format
msgid "Show QR code"
msgstr "Montri QR-kodon"

#: contents/ui/DelegateToolButtons.qml:57
#, kde-format
msgid "Edit contents"
msgstr "Redakti enhavojn"

#: contents/ui/DelegateToolButtons.qml:71
#, kde-format
msgid "Remove from history"
msgstr "Forigi el historio"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr "Konservi"

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Nuligi"

#: contents/ui/main.qml:30
#, kde-format
msgid "Clipboard Contents"
msgstr "Tonduja Enhavo"

#: contents/ui/main.qml:31 contents/ui/Menu.qml:134
#, kde-format
msgid "Clipboard is empty"
msgstr "Tondujo estas malplena"

#: contents/ui/main.qml:59
#, kde-format
msgid "Clear History"
msgstr "Malplenigi Historion"

#: contents/ui/main.qml:71
#, kde-format
msgid "Configure Clipboard…"
msgstr "Agordi tondujon…"

#: contents/ui/Menu.qml:134
#, kde-format
msgid "No matches"
msgstr "Neniuj alumetoj"

#: contents/ui/UrlItemDelegate.qml:107
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"

#~ msgid "The QR code is too large to be displayed"
#~ msgstr "La QR-kodo estas tro granda por esti montrata"
