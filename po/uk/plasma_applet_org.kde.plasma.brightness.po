# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-12-20 08:59+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.1\n"

#: package/contents/ui/BrightnessItem.qml:71
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Brightness and Color"
msgstr "Яскравість і колір"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Screen brightness at %1%"
msgstr "Яскравість екрана — %1%"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "Яскравість підсвічування клавіатури — %1%"

#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr "Нічні кольори вимкнено"

#: package/contents/ui/main.qml:107
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr "Нічні кольори — %1K"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Прокрутіть, щоб скоригувати яскравість екрана"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr "Клацання середньою для перемикання нічних кольорів"

#: package/contents/ui/main.qml:258
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "Налаштувати нічний режим…"

#: package/contents/ui/NightColorItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr "Вимкнено"

#: package/contents/ui/NightColorItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Немає доступу"

#: package/contents/ui/NightColorItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Не увімкнено"

#: package/contents/ui/NightColorItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Не запущено"

#: package/contents/ui/NightColorItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "Увімкнено"

#: package/contents/ui/NightColorItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "Вранішній перехід"

#: package/contents/ui/NightColorItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "День"

#: package/contents/ui/NightColorItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "Вечірній перехід"

#: package/contents/ui/NightColorItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "Ніч"

#: package/contents/ui/NightColorItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Configure…"
msgstr "Налаштувати…"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Enable and Configure…"
msgstr "Увімкнути і налаштувати…"

#: package/contents/ui/NightColorItem.qml:168
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "Перехід до дня завершується за:"

#: package/contents/ui/NightColorItem.qml:170
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "Перехід до ночі заплановано на:"

#: package/contents/ui/NightColorItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "Перехід до ночі завершується за:"

#: package/contents/ui/NightColorItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "Перехід до дня заплановано на:"

#: package/contents/ui/PopupDialog.qml:70
#, kde-format
msgid "Display Brightness"
msgstr "Яскравість екрана"

#: package/contents/ui/PopupDialog.qml:101
#, kde-format
msgid "Keyboard Brightness"
msgstr "Яскравість підсвічування клавіатури"

#: package/contents/ui/PopupDialog.qml:132
#, kde-format
msgid "Night Light"
msgstr "Нічні кольори"
